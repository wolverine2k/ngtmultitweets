import {Component, OnInit, OnDestroy} from '@angular/core';
@Component({
    selector: 'app-footer',
    templateUrl: 'app-footer.component.html',
    styleUrls: ['app-footer.component.scss'],
})
export class AppFooterComponent implements OnInit, OnDestroy {
    private mySiteURL = 'https://www.naresh.se/';
    constructor() {

    }

    ngOnInit() {

    }

    ngOnDestroy() {

    }
}
